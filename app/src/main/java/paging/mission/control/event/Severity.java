package paging.mission.control.event;

public enum Severity {
	
	RED_HIGH("RED HIGH"),
	RED_LOW("RED LOW");
	
	public final String value;
	
	private Severity(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
}
