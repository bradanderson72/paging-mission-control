package paging.mission.control.event;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class PagingEvent {

	private final Instant timestamp;
	private final int satelliteId;
	private final int redHighLimit;
	private final int redLowLimit;
	private final int yellowHighLimit;
	private final int yellowLowLimit;
	private final double rawValue;
	private final SatelliteComponent satelliteComponent;

	private PagingEvent(Instant timestamp, int satelliteId, int redHighLimit, int redLowLimit,
						int yellowHighLimit, int yellowLowLimit, double rawValue, SatelliteComponent satelliteComponent) {

		this.timestamp = timestamp;
		this.satelliteId = satelliteId;
		this.redHighLimit = redHighLimit;
		this.redLowLimit = redLowLimit;
		this.yellowHighLimit = yellowHighLimit;
		this.yellowLowLimit = yellowLowLimit;
		this.rawValue = rawValue;
		this.satelliteComponent = satelliteComponent;
	}

	/**
	 * |----0-----|------1-------|--------2-------|--------3----------|---------4--------|------5--------|------6----|-----7----|
	 * <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
	 * 
	 * @param splitLine a complete line delimited by a pipe | character
	 * @return a complete PagingEvent
	 */
	public static PagingEvent fromSplitLine(String[] splitLine) {

		Instant timestamp = LocalDateTime.parse(splitLine[0], DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS"))
										 .atOffset(ZoneOffset.UTC)
										 .toInstant();

		int satelliteId = Integer.parseInt(splitLine[1]);
		int redHighLimit = Integer.parseInt(splitLine[2]);
		int yellowHighLimit = Integer.parseInt(splitLine[3]);
		int yellowLowLimit = Integer.parseInt(splitLine[4]);
		int redLowLimit = Integer.parseInt(splitLine[5]);
		double rawValue = Double.parseDouble(splitLine[6]);
		SatelliteComponent satelliteComponent = SatelliteComponent.valueOf(splitLine[7]);

		return new PagingEvent(timestamp, satelliteId, redHighLimit, redLowLimit,
							   yellowHighLimit, yellowLowLimit, rawValue, satelliteComponent);
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	public int getSatelliteId() {
		return satelliteId;
	}

	public int getRedHighLimit() {
		return redHighLimit;
	}

	public int getRedLowLimit() {
		return redLowLimit;
	}

	public int getYellowHighLimit() {
		return yellowHighLimit;
	}

	public int getYellowLowLimit() {
		return yellowLowLimit;
	}

	public double getRawValue() {
		return rawValue;
	}

	public SatelliteComponent getSatelliteComponent() {
		return satelliteComponent;
	}

	@Override
	public String toString() {
		return "PagingEvent [timestamp=" + timestamp + ", satelliteId=" + satelliteId + ", redHighLimit=" + redHighLimit
				+ ", redLowLimit=" + redLowLimit + ", yellowHighLimit=" + yellowHighLimit + ", yellowLowLimit="
				+ yellowLowLimit + ", rawValue=" + rawValue + ", satelliteComponent=" + satelliteComponent + "]";
	}

}
