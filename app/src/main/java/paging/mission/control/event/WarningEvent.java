package paging.mission.control.event;

import java.time.Instant;

public class WarningEvent {

	private final int satelliteId;
	private final SatelliteComponent component;
	private final String severity;
	private final Instant timestamp;

	public WarningEvent(int satelliteId, SatelliteComponent component, String severity, Instant timestamp) {
		this.satelliteId = satelliteId;
		this.component = component;
		this.severity = severity;
		this.timestamp = timestamp;
	}
	
	public static WarningEvent fromPagingEventAndSeverity(PagingEvent pagingEvent, Severity severity) {
		
		return new WarningEvent(pagingEvent.getSatelliteId(), pagingEvent.getSatelliteComponent(), 
								severity.getValue(), pagingEvent.getTimestamp());
	}

	public int getSatelliteId() {
		return satelliteId;
	}

	public SatelliteComponent getComponent() {
		return component;
	}

	public String getSeverity() {
		return severity;
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	@Override
	public String toString() {
		return "WarningEvent [satelliteId=" + satelliteId + ", component=" + component + ", severity=" + severity
				+ ", timestamp=" + timestamp + "]";
	}

}
