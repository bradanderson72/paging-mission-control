package paging.mission.control;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import paging.mission.control.event.PagingEvent;
import paging.mission.control.event.SatelliteComponent;
import paging.mission.control.event.Severity;
import paging.mission.control.event.WarningEvent;

public class PagingMissionControllerService {

	public static final Duration DURATION_CAPACITY = Duration.ofMinutes(5);
	
	/**
	 * Processes a list of paging event lines. Each paging event line contains information
	 * pertaining to a telemetry status from a particular satellite.
	 * @param pagingEventLines The list of paging event lines to process
	 * @return Returns key, value pair map between satelliteId and a list of {@code PagingEvent}
	 */
	public Map<Integer, List<PagingEvent>> processPagingEvents(List<String> pagingEventLines) {
		return pagingEventLines.stream()
				.map(eventLine -> eventLine.split("[|]"))
				.map(PagingEvent::fromSplitLine)
				.collect(Collectors.groupingBy(PagingEvent::getSatelliteId));
	}
	
	/**
	 * Processes the paging event map between satelliteId and a list of {@code PagingEvent}
	 * For each list, we check for violations between {@code PagingEvent}s
	 * @param pagingEventMap the paging event key value pairs between satelliteId and a list of {@code PagingEvent}
	 * @return The list of violated paging events known as {@code WarningEvent}
	 */
	public List<WarningEvent> processEventMapForViolations(Map<Integer, List<PagingEvent>> pagingEventMap) {
		List<WarningEvent> warningEvents = new ArrayList<>();
		
		pagingEventMap.forEach((satelliteId, pagingEventList) -> warningEvents.addAll(checkForViolation(pagingEventList)));
		
		return warningEvents;
	}

	/**
	 * Reports via system out the {@Code WarningEvent}s that have violated certain conditions
	 * @param warningEvents the list of {@code WarningEvent}s to report
	 * @throws JsonProcessingException
	 */
	public static void reportWarningEvents(List<WarningEvent> warningEvents) throws JsonProcessingException {
		//Ensure we write dates as timestamps instead of epoch second
		ObjectMapper mapper = new ObjectMapper()
						.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false) 
						.registerModule(new JavaTimeModule());
		
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(warningEvents));
	}
	

	private List<WarningEvent> checkForViolation(List<PagingEvent> sortedPagingEvents) {

		List<PagingEvent> batteryPagingEvents = new ArrayList<>();
		List<PagingEvent> thermostatPagingEvents = new ArrayList<>();
		
		for (PagingEvent pagingEvent : sortedPagingEvents) {
			double rawValue = pagingEvent.getRawValue();
			SatelliteComponent satelliteComponent = pagingEvent.getSatelliteComponent();
			
			// If for the same satellite there are three battery voltage readings that are
			// under the red low limit within 5 minute interval
			if (satelliteComponent == SatelliteComponent.BATT && rawValue < pagingEvent.getRedLowLimit()) {
				batteryPagingEvents.add(pagingEvent);
			}
			
			// If for the same satellite there are three thermostat readings that exceed
			// the red high limit within a 5 minute interval
			if (satelliteComponent == SatelliteComponent.TSTAT && rawValue > pagingEvent.getRedHighLimit()) {
				thermostatPagingEvents.add(pagingEvent);
			}
		}

		List<WarningEvent> warningEvents = new ArrayList<>();
		warningEvents.addAll(findViolatedPagingEvents(batteryPagingEvents, Severity.RED_LOW));
		warningEvents.addAll(findViolatedPagingEvents(thermostatPagingEvents, Severity.RED_HIGH));
		
		return warningEvents;
	}

	private List<WarningEvent> findViolatedPagingEvents(List<PagingEvent> pagingEvents, Severity severity) {
		
		List<PagingEvent> sortedPagingEvents = pagingEvents.stream()
														   .sorted(Comparator.comparing(PagingEvent::getTimestamp))
														   .collect(Collectors.toList());
		Set<PagingEvent> violatedPagingEvents = new TreeSet<>(Comparator.comparing(PagingEvent::getTimestamp));
		
		List<WarningEvent> warningEvents = new ArrayList<>();
		
		for(int i = 0; i < sortedPagingEvents.size() - 1; i++) {
			PagingEvent pagingEventOne = sortedPagingEvents.get(i);
			PagingEvent pagingEventTwo = sortedPagingEvents.get(i+1);
			
			Duration durationBetweenEvents = Duration.between(pagingEventOne.getTimestamp(), pagingEventTwo.getTimestamp());
			
			boolean isOverDurationCapacity = durationBetweenEvents.toMinutes() < DURATION_CAPACITY.toMinutes();
			
			if(isOverDurationCapacity) {
				violatedPagingEvents.add(pagingEventOne);
				violatedPagingEvents.add(pagingEventTwo);
			}
			
			if(violatedPagingEvents.size() == 3) {
				Optional<PagingEvent> firstPagingEvent = violatedPagingEvents.stream().findFirst();
				firstPagingEvent.ifPresent(pagingEvent -> {
					WarningEvent warningEvent = WarningEvent.fromPagingEventAndSeverity(pagingEvent, severity);
					warningEvents.add(warningEvent);
				});
			}
		}
		return warningEvents;
	}
}
