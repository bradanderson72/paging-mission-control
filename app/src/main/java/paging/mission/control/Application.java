package paging.mission.control;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import paging.mission.control.event.PagingEvent;
import paging.mission.control.event.WarningEvent;

public class Application {
	
	public static void main(String[] args) throws IOException {
		
		List<String> pagingEventLines = new ArrayList<>();
		//try with resources auto-closes streams/scanners
		try(InputStream is = Application.class.getClassLoader().getResourceAsStream("input.txt");
				Scanner scanner = new Scanner(is)){
			while(scanner.hasNext()) {
				pagingEventLines.add(scanner.nextLine());
			}
		}
		
		PagingMissionControllerService controllerService = new PagingMissionControllerService();
		Map<Integer, List<PagingEvent>> processPagingEvents = controllerService.processPagingEvents(pagingEventLines);
		List<WarningEvent> warningEvents = controllerService.processEventMapForViolations(processPagingEvents);
		
		PagingMissionControllerService.reportWarningEvents(warningEvents);
	}
}
